<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Restate - Multipurpose HTML Template</title>
    <link rel="stylesheet" type="text/css" href="/asset/css/master.css">
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter45546153 = new Ya.Metrika({
                        id:45546153,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true,
                        webvisor:true,
                        trackHash:true
                    });
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/45546153" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
</head>
<body>

<!-- Loader -->
<div id="page-preloader"><span class="spinner"></span></div>
<!-- Loader end -->

<!-- Header -->
<header class="header">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-xs-10">
            </div>
            <div class="col-md-9 hidden-sm hidden-xs">
                <div class="main-menu">
                    <ul>
                        <li><a href="#about">About</a></li>
                        <li><a href="#contact">Contact</a></li>
                        <li><a class="phone-number" href="tel:32-234-112-32-32">{{$user->phone}}</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-2 hidden-md hidden-lg text-right">
                <div class="mobile-btn">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- Header -->

<!-- Mobile menu -->
<div class="mob-menu-wrapper hidden-md hidden-lg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="close-mob-menu">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </div>
                <div class="mobile-box-wrap">
                    <div class="mobile-box-flex">
                        <div class="mobile-menu">
                            <ul>
                                <li><a href="#about">About</a></li>
                                <li><a href="#contact">Contact</a></li>
                                <li><a class="phone-number" href="tel:32-234-112-32-32">{{$user->phone}}</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Mobile menu -->

<div class="wrapper">
    <div class="first-screen">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="greetings-box">
                        <div class="greetings-box-head">
                            <div class="main-name">
                                <h1 class="wow">{{$user->name}}</h1>
                            </div>
                            <div class="main-social">
                                <a target="_blank" class="fa fa-facebook" href="{{$contentOfAgent->facebook_url}}"></a>
                                <a target="_blank" class="fa fa-twitter" href="{{$contentOfAgent->zalo_url}}"></a>
                            </div>
                        </div>
                        <div class="greetings-box-body">
                            <p>{{$contentOfAgent->slogan}}</p>
                        </div>
                        <div class="greetings-box-footer">
                            <button id="free-consultation" class="site-btn">Get FREE consultation</button>
                            <div class="consultation-form-wrapper">
                                <form>
                                    <p>{{$user->phone}}</p>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="about" class="about-section">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-1">
                    <div class="about-image">
                        <img src="/asset/media/about-section/photo.jpg" alt="">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="about-text-box">
                        <h2 class="wow">hi, i’m {{$user->name}}!</h2>
                        <div class="about-me">
                            <p>{{$contentOfAgent->introduce}}</p>
                        </div>
                        <div class="sign-box">
                            <p>{{$user->name}}, Real Estate Agent</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="featured-properties">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-center">
                        <h2 class="wow">featured properties and projects</h2>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-5 col-xs-5 house-box-wrapper">
                    <a href="restate-single.html" class="house-box">
                        <div class="house-img">
                            <img src="/asset/media/houses/house-1.jpg" alt="">
                            <div class="price-box">
                                <div class="price">{{$property->price}} VND <i class="icofont icofont-star"></i></div>
                                <div class="price-sqft">{{$property->area}}</div>
                            </div>
                            <div class="characters text-right">
                                <p>{{$property->floor}} Tang</p>
                                <p>{{$property->bedroom}} Phong Ngu</p>
                                <p>{{$property->bathroom}} Phong Tam</p>
                            </div>
                        </div>
                        <div class="house-text-box">
                            <div class="house-name">
                                <h4>{{$property->title}}</h4>
                            </div>
                            <div class="house-short-description">
                                {{$property->address}}
                            </div>
                            <div class="more-details text-right">
                                <div class="more-text">More Details <i class="icofont icofont-long-arrow-right"></i></div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-7 col-xs-7 house-box-wrapper">
                    <a href="restate-single.html" class="house-box">
                        <div class="house-img">
                            <img src="/asset/media/houses/house-2.jpg" alt="">
                            <div class="price-box">
                                <div class="price">{{$project->name}}<i class="icofont icofont-star"></i></div>
                            </div>
                            <div class="characters text-right">
                                <p>{{$project->block}} Blocks</p>
                                <p>{{$project->floor}} Floors</p>
                                <p>{{$project->unit}} Units</p>
                            </div>
                        </div>
                        <div class="house-text-box">
                            <div class="house-name">
                                <h4>{{$project->name}}</h4>
                            </div>
                            <div class="house-short-description">
                                <p>{{$project->address}}</p>
                            </div>
                            <div class="house-short-description">
                                <p>Website: {{$project->website}} - Phone: {{$project->phone}}</p>
                            </div>
                            <div class="more-details text-right">
                                <div class="more-text">More Details <i class="icofont icofont-long-arrow-right"></i></div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="question-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="question-text">
                        <h3>Want to find the best apartments <br> in Barcelona? <a href="#contact">Contact me now!</a></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="contact" class="contact-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-center">
                        <h2 class="wow">contact me</h2>
                    </div>
                    <div class="description-section">
                        <p>{{$contentOfAgent->slogan}}</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-6 text-center contact-small-box">
                    <div class="phone-top">
                        <i class="icofont icofont-social-facebook"></i>
                        facebook
                    </div>
                    <div class="phone-bottom">
                        <a href="tel:84-255-242-24-24">{{$contentOfAgent->facebook_url}}</a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6 text-center contact-small-box">
                    <div class="phone-top">
                        <i class="icofont icofont-mobile-phone"></i>
                        mobile
                    </div>
                    <div class="phone-bottom">
                        <a href="tel:84-255-242-24-24">{{$user->phone}}</a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6 text-center contact-small-box">
                    <div class="phone-top">
                        <i class="icofont icofont-fax"></i>
                        zalo
                    </div>
                    <div class="phone-bottom">
                        <a href="tel:84-255-242-24-24">{{$contentOfAgent->zalo_url}}</a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6 text-center contact-small-box">
                    <div class="phone-top">
                        <i class="icofont icofont-email"></i>
                        e-mail
                    </div>
                    <div class="phone-bottom">
                        <a href="mailto:84-255-242-24-24">{{$user->email}}</a>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="form-wrapper">
                        <form action="http://previews.aspirity.com/restate/mail/contact_me.php" method="get" id="contact-form" novalidate>
                            <div class="form-group">
                                <input type="text" class="site-input name-input" placeholder="What is your name?" required>
                            </div>
                            <div class="form-group">
                                <input type="email" class="site-input email-input" placeholder="Your e-mail" required>
                            </div>
                            <div class="form-group">
                                <textarea class="site-input textarea" placeholder="Write your message here..." required></textarea>
                            </div>
                            <div class="text-center">
                                <input type="submit" class="site-btn" value="Send message">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="copyright text-center">MGI Global 2018. All rights reserved.</div>
            </div>
        </div>
    </div>
</footer>


<script src="/asset/js/jquery-2.2.3.min.js"></script>
<script src="/asset/js/bootstrap.min.js"></script>

<script src="/asset/js/jquery.placeholder.min.js"></script>

<!--Slick slider-->
<script src="/asset/assets/slick-1.6.0/slick/slick.min.js"></script>

<!--Smooth Scroll-->
<script src="/asset/assets/smoothscroll-for-websites/SmoothScroll.js"></script>

<!--jqBootstrapValidation-->
<script src="/asset/assets/contact/jqBootstrapValidation.js"></script>
<script src="/asset/assets/contact/contact_me.js"></script>

<!--Map-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBc0h6uJYApeFPd8S8Bd3eiGrHPcodSfXo" async defer></script>
<script src="/asset/js/map.js"></script>

<!--Custom select-->
<script src="/asset/js/custom-select.js"></script>

<!--jQuery UI slider-->
<script src="/asset/assets/jquery-ui-1.12.1.slider/jquery-ui.min.js"></script>
<script src="/asset/assets/jquery-ui-touch-punch-master/jquery.ui.touch-punch.min.js"></script>

<!--Isotope-->
<script src="/asset/assets/isotope/isotope.pkgd.min.js"></script>

<!--Textify-->
<script src="/asset/js/textify.js"></script>

<!--WOW-->
<script src="/asset/js/wow.min.js"></script>

<!--THEME-->
<script src="/asset/js/theme.js"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-139591409-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-139591409-1');
</script>

</body>

</html>
