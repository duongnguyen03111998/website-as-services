<!doctype html>
<html class="no-js" lang="en">


<!-- Mirrored from dreamer.victheme.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 11 Jul 2019 07:17:53 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Index</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">



    <!-- Bootsrap css-->
    <link rel="stylesheet" href="/asset2/css/bootstrap.min.css">
    <!-- owl carousel css-->
    <link rel="stylesheet" href="/asset2/css/owl.carousel.css">
    <link rel="stylesheet" href="/asset2/css/owl.theme.css">
    <!-- Font Awesome css-->
    <link rel="stylesheet" href="/asset2/css/font-awesome.min.css">
    <!-- Bootstrap Nav css-->
    <link rel="stylesheet" href="/asset2/css/bootsnav.css">
    <!-- style css-->
    <link rel="stylesheet" href="/asset2/css/style.css">
</head>

<body class="home-element">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- Add your site or application content here -->
<!-- // Menu  -->
<header>
    <!-- // top header  -->
    <div class="top-header">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-12 col-xs-12 col-lg-4">
                    <div class="logo">
                        <a href="#"><img src="/asset2/images/logo.png" alt=""></a>
                    </div>
                </div>
                <div class="col-md-8 col-sm-12 col-xs-12 col-lg-8">
                    <div class="communication-info text-right">

                        <ul>
                            <li class="top-list-icon"><i class="fa fa-phone top-icon info-icon"></i>00 (386) 49 511 868</li>
                            <li class="top-list-icon"><i class="fa fa-envelope info-icon top-icon"></i>asmindream@home.com</li>
                            <li class="top-icon"><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li class="top-icon"><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li class="top-icon"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li class="top-icon"><a href="#"><i class="fa fa-wifi"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- // top header end -->

    <!-- // header -->
    <div class="header-menu pr">
        <div class="container-fluid">
            <div class="row">

                <nav class="navbar navbar-default bootsnav">
                    <div class="container">
                        <!-- Start Header Navigation -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                                <i class="fa fa-bars"></i>
                            </button>
                        </div>
                        <!-- End Header Navigation -->
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="navbar-menu">
                            <ul class="nav header-nav navbar-nav navbar-left" data-in="fadeInDown" data-out="fadeOutUp">
                                <li><a href="index-2.html">Home</a></li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" >Pages</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="agent-grid.html">Agent grid</a></li>
                                        <li><a href="agent-list.html">Agent list</a></li>
                                        <li><a href="blog-grid.html">Blog grid</a></li>
                                        <li><a href="blog-list.html">Blog list</a></li>
                                        <li><a href="property-grid.html">Property grid</a></li>
                                        <li><a href="property-list.html">Property list</a></li>
                                        <li><a href="property-masonary.html">Property masonary</a></li>
                                        <li><a href="single-blog.html">Single blog</a></li>
                                        <li><a href="single-agent.html">Single agent</a></li>
                                        <li><a href="single-property.html">Single property</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" >Headline</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="heading.html">ShortCode Hdeading</a></li>
                                        <li><a href="header.html">ShortCode Header</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">Template</a></li>
                                <li><a href="#">Contact Us</a></li>
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </div>
                </nav>
            </div>
        </div>
    </div><!-- // header end -->
</header>
<!--// banner -->
<div class="main-slider">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12 no-padding">
                <div class="author-level text-center">
                    <img src="/asset2/images/author.png" alt="">
                    <h3>Antonio Contella</h3>
                    <span class="designation p-b-25 b-1px">Heartly Real Estate</span>
                    <div class="social-link-area p-t-23">
                        <p>Bolf moon pabst kombucha, photo booth roof party literally hashtag lumbersexual tacos sustainable pop-up readymade affogato,</p>
                        <ul class="m-t-28 p-b-30 b-1px">
                            <li><a href="#"><i class="fa fa-facebook social"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter social"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin social"></i></a></li>
                            <li><a href="#"><i class="fa fa-behance social"></i></a></li>
                        </ul>
                    </div>
                    <button class="btn-btn view-btn w-160 m-t-30" type="button">VISIT HIS PROFILE</button>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 no-padding">
                <div class="author-select-form">
                    <div class="select-form m-width pull-right">
                        <div class="drop-menu m-b-15">
                            <div class="select">
                                <span>Any House</span>
                                <i class="fa fa-chevron-down"></i>
                            </div>

                            <form>
                                <input class="genderVal" type="hidden" name="gender">
                            </form>

                            <ul class="dropeddown">
                                <li id="male-house">Luxay House</li>
                                <li id="female-house">Dream House</li>
                            </ul>
                        </div>
                        <div class="drop-menu m-b-15">
                            <div class="select">
                                <span>Any Status</span>
                                <i class="fa fa-chevron-down"></i>
                            </div>

                            <form>
                                <input class="genderVal" type="hidden" name="gender">
                            </form>

                            <ul class="dropeddown">
                                <li id="male">Male</li>
                                <li id="female">Female</li>
                            </ul>
                        </div>
                        <div class="drop-menu m-b-15">
                            <div class="select">
                                <span>Any Bathroom</span>
                                <i class="fa fa-chevron-down"></i>
                            </div>

                            <form>
                                <input class="genderVal" type="hidden" name="gender">
                            </form>

                            <ul class="dropeddown">
                                <li id="gent">Gents Bathroom</li>
                                <li id="ladis">Ladis Bathroom</li>
                            </ul>
                        </div>
                        <div class="form-group w-300 m-b-15">
                            <form>
                                <input class="custom-input form-control" type="text" name="name" placeholder="Type address">
                            </form>
                        </div>
                        <div class="form-group m-b-0">
                            <form>
                                <p class="m-b-0"><input class="custom-input form-control pull-left w-142 m-r-16" type="text" name="name" placeholder="Minimum..."></p>
                                <p class="m-b-0"><input class="custom-input form-control w-142" type="text" name="name" placeholder="Maximum..."></p>
                            </form>
                        </div>
                        <div class="text-center">
                            <button class="btn-btn view-btn w-160 m-t-30" type="button">Search Now</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--// banner end -->

<!--// about us-->
<div class="about m-t-80">
    <div class="container">
        <div class="row">
            <div class="col-md-4 no-padding col-sm-12 col-xs-12 col-lg-4">
                <div class="about-img-content">
                    <img class="img-responsive" src="/asset2/images/about-1.jpg" alt="">
                </div>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12 col-lg-8">
                <div class="agent-text-content p-8">
                    <span class="designation">Featured Agent</span>
                    <h4 class="horizontal-line">Antonio Contella</h4>
                    <p class="m-t-32 m-b-15">Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                    <p class="m-b-20">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque</p>
                    <div class="info">
                        <ul>
                            <li> Address: <span> Rochelle Park, NJ 07662</span></li>
                            <li>Phone: <span> 0386 49 511 868</span></li>
                            <li>skype: <span> deam.home</span></li>
                            <li>
                                <div>
                                    <a href="#"><i class="fa fa-facebook social-icon-link"></i></a>
                                    <a href="#"><i class="fa fa-twitter social-icon-link"></i></a>
                                    <a href="#"><i class="fa fa-linkedin social-icon-link"></i></a>
                                    <a href="#"><i class="fa fa-rss social-icon-link"></i></a>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <button type="button" class="btn-btn view-btn w-160 m-t-27">VISIT HIS PROFILE</button>

                </div>
            </div>
        </div>
    </div>
</div>
<!--// about us end-->

<!--// bg section -->
<div class="home-bg">
    <div class="container">
        <div class="bg-content">
            <div class="overlay"></div>
            <div class="row">
                <div class="col-md-9">
                    <h3><span class="bold">VILLA LUX</span> POOL In Grarden The best ever you have seen!</h3>
                </div>
                <div class="col-md-3">
                    <div class="text-right">
                        <button type="button" class="btn-btn view-btn w-160">VISIT HIS PROFILE</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--// bg section end-->
<!--//masonary-->
<div class="masonary m-b-50">
    <div class="container">
        <div class="heading text-center">
            <h3 class="heading-title">Featured Property</h3>
            <p class="sub-head">Check here our featured property post</p>
        </div>
        <div class="row m-t-47">
            <div class="controls">
                <div class="col-md-12 col-xs-12 <text-center></text-center>">
                    <br/>
                    <div class="masonary-grid-menu text-center m-b-50">
                        <a>
                            <div class="filter white-border" data-filter="all">Show All</div>
                        </a>
                        <a>
                            <div class="white-border filter" data-filter=".category-2">Houses</div>
                        </a>
                        <a>
                            <div class="white-border filter" data-filter=".category-3">Villa</div>
                        </a>
                        <a>
                            <div class="white-border filter" data-filter=".category-4">Appartments</div>
                        </a>
                        <a>
                            <div class="white-border filter" data-filter=".category-5">Luxury</div>
                        </a>
                    </div>
                </div>
            </div>
            <div id="product-grid" class="product-grid">
                <div class="mix category-1 category-4 col-md-4 col-xs-12 col-sm-6" data-myorder="9">
                    <div class="agent-img-content">
                        <img class="img-responsive" src="/asset2/images/11.jpg" alt="">
                        <div class="rent">For Rent</div>
                        <div class="agent-list-hover">
                            <a href="#"><i class="fa fa-search pop-up-icon"></i></a>
                        </div>
                    </div>
                    <div class="masonary-content bg-ash m-b-30">
                        <div class="info p-t-20 p-b-20 b-b-1px">
                            <ul class="text-center">
                                <li><i class="fa fa-map-marker info-icon"></i> Space <span> 80m2</span></li>
                                <li><i class="fa fa-mobile info-icon"></i>Bedroom <span> 3</span></li>
                                <li><i class="fa fa-mobile info-icon"></i>Bathroom <span> 1</span></li>
                            </ul>
                        </div>
                        <div class="p-t-15">
                            <span class="designation">Rochelle Park, NJ 07662</span>
                            <h4 class="p-b-25">Luxury Villa Pool</h4>
                        </div>
                        <div class="dolar-bottom">
                            <p>$450.000</p>
                            <span class="month">Per Month</span>
                        </div>
                    </div>
                </div>
                <div class="mix category-3 category-3 col-md-4 col-xs-12 col-sm-6" data-myorder="2">

                    <div class="agent-img-content">
                        <img class="img-responsive" src="/asset2/images/11.jpg" alt="">
                        <div class="rent">For Rent</div>
                        <div class="agent-list-hover">
                            <a href="#"><i class="fa fa-search pop-up-icon"></i></a>
                        </div>
                    </div>
                    <div class="masonary-content bg-ash m-b-30">
                        <div class="info p-t-20 p-b-20 b-b-1px">
                            <ul class="text-center">
                                <li><i class="fa fa-map-marker info-icon"></i> Space <span> 80m2</span></li>
                                <li><i class="fa fa-mobile info-icon"></i>Bedroom <span> 3</span></li>
                                <li><i class="fa fa-mobile info-icon"></i>Bathroom <span> 1</span></li>
                            </ul>
                        </div>
                        <div class="p-t-15">
                            <span class="designation">Rochelle Park, NJ 07662</span>
                            <h4 class="p-b-25">Luxury Villa Pool</h4>
                        </div>
                        <div class="dolar-bottom">
                            <p>$450.000</p>
                            <span class="month">Per Month</span>
                        </div>
                    </div>

                </div>
                <div class="mix category-2 col-md-4 col-xs-12 col-sm-6" data-myorder="3">

                    <div class="agent-img-content">
                        <img class="img-responsive" src="/asset2/images/11.jpg" alt="">
                        <div class="rent">For Rent</div>
                        <div class="agent-list-hover">
                            <a href="#"><i class="fa fa-search pop-up-icon"></i></a>
                        </div>
                    </div>
                    <div class="masonary-content bg-ash m-b-30">
                        <div class="info p-t-20 p-b-20 b-b-1px">
                            <ul class="text-center">
                                <li><i class="fa fa-map-marker info-icon"></i> Space <span> 80m2</span></li>
                                <li><i class="fa fa-mobile info-icon"></i>Bedroom <span> 3</span></li>
                                <li><i class="fa fa-mobile info-icon"></i>Bathroom <span> 1</span></li>
                            </ul>
                        </div>
                        <div class="p-t-15">
                            <span class="designation">Rochelle Park, NJ 07662</span>
                            <h4 class="p-b-25">Luxury Villa Pool</h4>
                        </div>
                        <div class="dolar-bottom">
                            <p>$450.000</p>
                            <span class="month">Per Month</span>
                        </div>
                    </div>

                </div>
                <div class="mix category-1 category-4 col-md-4 col-xs-12 col-sm-6" data-myorder="9">
                    <div class="agent-img-content">
                        <img class="img-responsive" src="/asset2/images/11.jpg" alt="">
                        <div class="rent">For Rent</div>
                        <div class="agent-list-hover">
                            <a href="#"><i class="fa fa-search pop-up-icon"></i></a>
                        </div>
                    </div>
                    <div class="masonary-content bg-ash m-b-30">
                        <div class="info p-t-20 p-b-20 b-b-1px">
                            <ul class="text-center">
                                <li><i class="fa fa-map-marker info-icon"></i> Space <span> 80m2</span></li>
                                <li><i class="fa fa-mobile info-icon"></i>Bedroom <span> 3</span></li>
                                <li><i class="fa fa-mobile info-icon"></i>Bathroom <span> 1</span></li>
                            </ul>
                        </div>
                        <div class="p-t-15">
                            <span class="designation">Rochelle Park, NJ 07662</span>
                            <h4 class="p-b-25">Luxury Villa Pool</h4>
                        </div>
                        <div class="dolar-bottom">
                            <p>$450.000</p>
                            <span class="month">Per Month</span>
                        </div>
                    </div>
                </div>
                <div class="mix category-1 category-4 col-md-4 col-xs-12 col-sm-6" data-myorder="9">
                    <div class="agent-img-content">
                        <img class="img-responsive" src="/asset2/images/11.jpg" alt="">
                        <div class="rent">For Rent</div>
                        <div class="agent-list-hover">
                            <a href="#"><i class="fa fa-search pop-up-icon"></i></a>
                        </div>
                    </div>
                    <div class="masonary-content bg-ash m-b-30">
                        <div class="info p-t-20 p-b-20 b-b-1px">
                            <ul class="text-center">
                                <li><i class="fa fa-map-marker info-icon"></i> Space <span> 80m2</span></li>
                                <li><i class="fa fa-mobile info-icon"></i>Bedroom <span> 3</span></li>
                                <li><i class="fa fa-mobile info-icon"></i>Bathroom <span> 1</span></li>
                            </ul>
                        </div>
                        <div class="p-t-15">
                            <span class="designation">Rochelle Park, NJ 07662</span>
                            <h4 class="p-b-25">Luxury Villa Pool</h4>
                        </div>
                        <div class="dolar-bottom">
                            <p>$450.000</p>
                            <span class="month">Per Month</span>
                        </div>
                    </div>
                </div>
                <div class="mix category-1 category-5 col-md-4 col-xs-12 col-sm-6" data-myorder="9">
                    <div class="agent-img-content">
                        <img class="img-responsive" src="/asset2/images/11.jpg" alt="">
                        <div class="rent">For Rent</div>
                        <div class="agent-list-hover">
                            <a href="#"><i class="fa fa-search pop-up-icon"></i></a>
                        </div>
                    </div>
                    <div class="masonary-content bg-ash m-b-30">
                        <div class="info p-t-20 p-b-20 b-b-1px">
                            <ul class="text-center">
                                <li><i class="fa fa-map-marker info-icon"></i> Space <span> 80m2</span></li>
                                <li><i class="fa fa-mobile info-icon"></i>Bedroom <span> 3</span></li>
                                <li><i class="fa fa-mobile info-icon"></i>Bathroom <span> 1</span></li>
                            </ul>
                        </div>
                        <div class="p-t-15">
                            <span class="designation">Rochelle Park, NJ 07662</span>
                            <h4 class="p-b-25">Luxury Villa Pool</h4>
                        </div>
                        <div class="dolar-bottom">
                            <p>$450.000</p>
                            <span class="month">Per Month</span>
                        </div>
                    </div>
                </div>
                <div class="gap"></div>
                <div class="gap"></div>
            </div>
        </div>
    </div>
</div>
<!--// masonry end -->

<!--// testimonial -->
<div class="testimonial p-t-80 p-b-80">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="testimonial-slider text-center">
                    <div class="item-content">
                        <div class="star-rating title-line">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                        </div>
                        <p class="m-t-50">Pork belly ramps squid migas 90's knausgaard jean shorts, aesthetic microdosing mustache chartreuse yr chambray iPhone jean shorts swag.</p>
                        <div class="byer m-t-32">
                            <h4>Johnson Family</h4>
                            <span>Home Byers</span>
                        </div>
                    </div>
                    <div class="item-content">
                        <div class="star-rating title-line">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                        </div>
                        <p class="m-t-50">Pork belly ramps squid migas 90's knausgaard jean shorts, aesthetic microdosing mustache chartreuse yr chambray iPhone jean shorts swag.</p>
                        <div class="byer m-t-30">
                            <h4>Johnson Family</h4>
                            <span>Home Byers</span>
                        </div>
                    </div>
                    <div class="item-content">
                        <div class="star-rating title-line">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                        </div>
                        <p class="m-t-50">Pork belly ramps squid migas 90's knausgaard jean shorts, aesthetic microdosing mustache chartreuse yr chambray iPhone jean shorts swag.</p>
                        <div class="byer m-t-30">
                            <h4>Johnson Family</h4>
                            <span>Home Byers</span>
                        </div>
                    </div>
                    <div class="item-content">
                        <div class="star-rating title-line">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                        </div>
                        <p class="m-t-50">Pork belly ramps squid migas 90's knausgaard jean shorts, aesthetic microdosing mustache chartreuse yr chambray iPhone jean shorts swag.</p>
                        <div class="byer m-t-30">
                            <h4>Johnson Family</h4>
                            <span>Home Byers</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="img-content">
            <img class="img-responsive" src="/asset2/images/family.png" alt="">
        </div>
    </div>
</div>
<!--// testimonial end -->

<!--// Feature Property-->
<div class="feature-property m-t-60 m-b-80">
    <div class="container">
        <div class="heading text-center">
            <h3 class="heading-title">Featured Property</h3>
            <p class="sub-head">Check here our featured property post</p>
        </div>
        <div class="row m-t-75">
            <div class="col-md-2 p-r-0 p-l-0">
                <!-- Nav tabs -->
                <ul class="nav garden-list" role="tablist">
                    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Garden House</a></li>
                    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Big Garage</a></li>
                    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Living Room</a></li>
                    <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Bed Room</a></li>
                    <li role="presentation"><a href="#bathroom" aria-controls="settings" role="tab" data-toggle="tab">Bath Room</a></li>
                    <li role="presentation"><a href="#swiming" aria-controls="settings" role="tab" data-toggle="tab">Swiming Pool</a></li>
                    <li role="presentation"><a class="visit-room" href="#visit" aria-controls="settings" role="tab" data-toggle="tab">Visit House</a></li>
                </ul>
            </div>
            <div class="col-md-10">
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="home">
                        <div class="row bg-ash">
                            <div class="col-md-7 p-l-0">
                                <div class="agent-text-content living-room">
                                    <h4 class="horizontal-line">Antonio Contella</h4>
                                    <p class="m-t-30">Bolf moon pabst kombucha, photo booth roof party literally hashtag viny mlkshk cred scenester synth 8-bit. Four loko cray wayfarers.</p>
                                    <ul>
                                        <li class="m-t-25">
                                            <span class="item-icon"><i class="fa fa-lightbulb-o"></i></span>
                                            <div class="feature-content-item m-l-12">
                                                <h5>Lighten Village</h5>
                                                <p>Poutine squid pabst echo park. Messenger bag chicharrones axe.</p>
                                            </div>
                                        </li>
                                        <li class="m-t-5">
                                            <span class="item-icon"><i class="fa fa-lightbulb-o"></i></span>
                                            <div class="feature-content-item m-l-12">
                                                <h5>Lighten Village</h5>
                                                <p>Hard hashtag bicycle rights typewriter hammock semiotic salvia.</p>
                                            </div>
                                        </li>
                                        <li class="m-t-5">
                                            <span class="item-icon"><i class="fa fa-lightbulb-o"></i></span>
                                            <div class="feature-content-item m-l-12">
                                                <h5>Lighten Village</h5>
                                                <p>Venmo celiac a park intelligentsia sustainable butch pug church.</p>
                                            </div>
                                        </li>
                                        <li class="m-t-5">
                                            <span class="item-icon"><i class="fa fa-lightbulb-o"></i></span>
                                            <div class="feature-content-item m-l-12">
                                                <h5>Lighten Village</h5>
                                                <p>Letterpress lo-fi retro put a bird on it cold-pressed kickstarter.</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-5 no-padding">
                                <div class="img-content-right">
                                    <img class="img-responsive" src="/asset2/images/1.jpg" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="profile">
                        <div class="row bg-ash">
                            <div class="col-md-7 p-l-0">
                                <div class="agent-text-content living-room">
                                    <h4 class="horizontal-line">Antonio Contella</h4>
                                    <p class="m-t-30">Bolf moon pabst kombucha, photo booth roof party literally hashtag viny mlkshk cred scenester synth 8-bit. Four loko cray wayfarers.</p>
                                    <ul>
                                        <li class="m-t-25">
                                            <span class="item-icon"><i class="fa fa-lightbulb-o"></i></span>
                                            <div class="feature-content-item m-l-12">
                                                <h5>Lighten Village</h5>
                                                <p>Poutine squid pabst echo park. Messenger bag chicharrones axe.</p>
                                            </div>
                                        </li>
                                        <li class="m-t-5">
                                            <span class="item-icon"><i class="fa fa-lightbulb-o"></i></span>
                                            <div class="feature-content-item m-l-12">
                                                <h5>Lighten Village</h5>
                                                <p>Poutine squid pabst echo park. Messenger bag chicharrones axe.</p>
                                            </div>
                                        </li>
                                        <li class="m-t-5">
                                            <span class="item-icon"><i class="fa fa-lightbulb-o"></i></span>
                                            <div class="feature-content-item m-l-12">
                                                <h5>Lighten Village</h5>
                                                <p>Poutine squid pabst echo park. Messenger bag chicharrones axe.</p>
                                            </div>
                                        </li>
                                        <li class="m-t-5">
                                            <span class="item-icon"><i class="fa fa-lightbulb-o"></i></span>
                                            <div class="feature-content-item m-l-12">
                                                <h5>Lighten Village</h5>
                                                <p>Poutine squid pabst echo park. Messenger bag chicharrones axe.</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-5 no-padding">
                                <div class="img-content-right">
                                    <img class="img-responsive" src="/asset2/images/1.jpg" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="messages">
                        <div class="row bg-ash">
                            <div class="col-md-7 p-l-0">
                                <div class="agent-text-content living-room">
                                    <h4 class="horizontal-line">Antonio Contella</h4>
                                    <p class="m-t-30">Bolf moon pabst kombucha, photo booth roof party literally hashtag viny mlkshk cred scenester synth 8-bit. Four loko cray wayfarers.</p>
                                    <ul>
                                        <li class="m-t-25">
                                            <span class="item-icon"><i class="fa fa-lightbulb-o"></i></span>
                                            <div class="feature-content-item m-l-12">
                                                <h5>Lighten Village</h5>
                                                <p>Poutine squid pabst echo park. Messenger bag chicharrones axe.</p>
                                            </div>
                                        </li>
                                        <li class="m-t-5">
                                            <span class="item-icon"><i class="fa fa-lightbulb-o"></i></span>
                                            <div class="feature-content-item m-l-12">
                                                <h5>Lighten Village</h5>
                                                <p>Poutine squid pabst echo park. Messenger bag chicharrones axe.</p>
                                            </div>
                                        </li>
                                        <li class="m-t-5">
                                            <span class="item-icon"><i class="fa fa-lightbulb-o"></i></span>
                                            <div class="feature-content-item m-l-12">
                                                <h5>Lighten Village</h5>
                                                <p>Poutine squid pabst echo park. Messenger bag chicharrones axe.</p>
                                            </div>
                                        </li>
                                        <li class="m-t-5">
                                            <span class="item-icon"><i class="fa fa-lightbulb-o"></i></span>
                                            <div class="feature-content-item m-l-12">
                                                <h5>Lighten Village</h5>
                                                <p>Poutine squid pabst echo park. Messenger bag chicharrones axe.</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-5 no-padding">
                                <div class="img-content-right">
                                    <img class="img-responsive" src="/asset2/images/1.jpg" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="settings">
                        <div class="row bg-ash">
                            <div class="col-md-7 p-l-0">
                                <div class="agent-text-content living-room">
                                    <h4 class="horizontal-line">Antonio Contella</h4>
                                    <p class="m-t-30">Bolf moon pabst kombucha, photo booth roof party literally hashtag viny mlkshk cred scenester synth 8-bit. Four loko cray wayfarers.</p>
                                    <ul>
                                        <li class="m-t-25">
                                            <span class="item-icon"><i class="fa fa-lightbulb-o"></i></span>
                                            <div class="feature-content-item m-l-12">
                                                <h5>Lighten Village</h5>
                                                <p>Poutine squid pabst echo park. Messenger bag chicharrones axe.</p>
                                            </div>
                                        </li>
                                        <li class="m-t-5">
                                            <span class="item-icon"><i class="fa fa-lightbulb-o"></i></span>
                                            <div class="feature-content-item m-l-12">
                                                <h5>Lighten Village</h5>
                                                <p>Poutine squid pabst echo park. Messenger bag chicharrones axe.</p>
                                            </div>
                                        </li>
                                        <li class="m-t-5">
                                            <span class="item-icon"><i class="fa fa-lightbulb-o"></i></span>
                                            <div class="feature-content-item m-l-12">
                                                <h5>Lighten Village</h5>
                                                <p>Poutine squid pabst echo park. Messenger bag chicharrones axe.</p>
                                            </div>
                                        </li>
                                        <li class="m-t-5">
                                            <span class="item-icon"><i class="fa fa-lightbulb-o"></i></span>
                                            <div class="feature-content-item m-l-12">
                                                <h5>Lighten Village</h5>
                                                <p>Poutine squid pabst echo park. Messenger bag chicharrones axe.</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-5 no-padding">
                                <div class="img-content-right">
                                    <img class="img-responsive" src="/asset2/images/1.jpg" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="bathroom">
                        <div class="row bg-ash">
                            <div class="col-md-7 p-l-0">
                                <div class="agent-text-content living-room">
                                    <h4 class="horizontal-line">Antonio Contella</h4>
                                    <p class="m-t-30">Bolf moon pabst kombucha, photo booth roof party literally hashtag viny mlkshk cred scenester synth 8-bit. Four loko cray wayfarers.</p>
                                    <ul>
                                        <li class="m-t-25">
                                            <span class="item-icon"><i class="fa fa-lightbulb-o"></i></span>
                                            <div class="feature-content-item m-l-12">
                                                <h5>Lighten Village</h5>
                                                <p>Poutine squid pabst echo park. Messenger bag chicharrones axe.</p>
                                            </div>
                                        </li>
                                        <li class="m-t-5">
                                            <span class="item-icon"><i class="fa fa-lightbulb-o"></i></span>
                                            <div class="feature-content-item m-l-12">
                                                <h5>Lighten Village</h5>
                                                <p>Poutine squid pabst echo park. Messenger bag chicharrones axe.</p>
                                            </div>
                                        </li>
                                        <li class="m-t-5">
                                            <span class="item-icon"><i class="fa fa-lightbulb-o"></i></span>
                                            <div class="feature-content-item m-l-12">
                                                <h5>Lighten Village</h5>
                                                <p>Poutine squid pabst echo park. Messenger bag chicharrones axe.</p>
                                            </div>
                                        </li>
                                        <li class="m-t-5">
                                            <span class="item-icon"><i class="fa fa-lightbulb-o"></i></span>
                                            <div class="feature-content-item m-l-12">
                                                <h5>Lighten Village</h5>
                                                <p>Poutine squid pabst echo park. Messenger bag chicharrones axe.</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-5 no-padding">
                                <div class="img-content-right">
                                    <img class="img-responsive" src="/asset2/images/1.jpg" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="swiming">
                        <div class="row bg-ash">
                            <div class="col-md-7 p-l-0">
                                <div class="agent-text-content living-room">
                                    <h4 class="horizontal-line">Antonio Contella</h4>
                                    <p class="m-t-30">Bolf moon pabst kombucha, photo booth roof party literally hashtag viny mlkshk cred scenester synth 8-bit. Four loko cray wayfarers.</p>
                                    <ul>
                                        <li class="m-t-25">
                                            <span class="item-icon"><i class="fa fa-lightbulb-o"></i></span>
                                            <div class="feature-content-item m-l-12">
                                                <h5>Lighten Village</h5>
                                                <p>Poutine squid pabst echo park. Messenger bag chicharrones axe.</p>
                                            </div>
                                        </li>
                                        <li class="m-t-5">
                                            <span class="item-icon"><i class="fa fa-lightbulb-o"></i></span>
                                            <div class="feature-content-item m-l-12">
                                                <h5>Lighten Village</h5>
                                                <p>Poutine squid pabst echo park. Messenger bag chicharrones axe.</p>
                                            </div>
                                        </li>
                                        <li class="m-t-5">
                                            <span class="item-icon"><i class="fa fa-lightbulb-o"></i></span>
                                            <div class="feature-content-item m-l-12">
                                                <h5>Lighten Village</h5>
                                                <p>Poutine squid pabst echo park. Messenger bag chicharrones axe.</p>
                                            </div>
                                        </li>
                                        <li class="m-t-5">
                                            <span class="item-icon"><i class="fa fa-lightbulb-o"></i></span>
                                            <div class="feature-content-item m-l-12">
                                                <h5>Lighten Village</h5>
                                                <p>Poutine squid pabst echo park. Messenger bag chicharrones axe.</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-5 no-padding">
                                <div class="img-content-right">
                                    <img class="img-responsive" src="/asset2/images/1.jpg" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="visit">
                        <div class="row bg-ash">
                            <div class="col-md-7 p-l-0">
                                <div class="agent-text-content living-room">
                                    <h4 class="horizontal-line">Antonio Contella</h4>
                                    <p class="m-t-30">Bolf moon pabst kombucha, photo booth roof party literally hashtag viny mlkshk cred scenester synth 8-bit. Four loko cray wayfarers.</p>
                                    <ul>
                                        <li class="m-t-25">
                                            <span class="item-icon"><i class="fa fa-lightbulb-o"></i></span>
                                            <div class="feature-content-item m-l-12">
                                                <h5>Lighten Village</h5>
                                                <p>Poutine squid pabst echo park. Messenger bag chicharrones axe.</p>
                                            </div>
                                        </li>
                                        <li class="m-t-5">
                                            <span class="item-icon"><i class="fa fa-lightbulb-o"></i></span>
                                            <div class="feature-content-item m-l-12">
                                                <h5>Lighten Village</h5>
                                                <p>Poutine squid pabst echo park. Messenger bag chicharrones axe.</p>
                                            </div>
                                        </li>
                                        <li class="m-t-5">
                                            <span class="item-icon"><i class="fa fa-lightbulb-o"></i></span>
                                            <div class="feature-content-item m-l-12">
                                                <h5>Lighten Village</h5>
                                                <p>Poutine squid pabst echo park. Messenger bag chicharrones axe.</p>
                                            </div>
                                        </li>
                                        <li class="m-t-5">
                                            <span class="item-icon"><i class="fa fa-lightbulb-o"></i></span>
                                            <div class="feature-content-item m-l-12">
                                                <h5>Lighten Village</h5>
                                                <p>Poutine squid pabst echo park. Messenger bag chicharrones axe.</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-5 no-padding">
                                <div class="img-content-right">
                                    <img class="img-responsive" src="/asset2/images/1.jpg" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--// Feature Property End-->

<!--// our client -->
<div class="our-client p-t-35 p-b-50">
    <div class="container">
        <div class="heading text-center">
            <h3 class="heading-title">Our Partners</h3>
            <p class="sub-head">Check here our partners</p>
        </div>
        <div class="row">
            <div id="client">
                <div class="col-xs-12">
                    <div class="item-area m-t-65 text-center bg-white p-20">
                        <img src="/asset2/images/partner.png" alt="">
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="item-area m-t-65 text-center bg-white p-20">
                        <img src="/asset2/images/partner.png" alt="">
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="item-area m-t-65 text-center bg-white p-20">
                        <img src="/asset2/images/partner.png" alt="">
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="item-area m-t-65 text-center bg-white p-20">
                        <img src="/asset2/images/partner.png" alt="">
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="item-area m-t-65 text-center bg-white p-20">
                        <img src="/asset2/images/partner.png" alt="">
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="item-area m-t-65 text-center bg-white p-20">
                        <img src="/asset2/images/partner.png" alt="">
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="item-area m-t-65 text-center bg-white p-20">
                        <img src="/asset2/images/partner.png" alt="">
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="item-area m-t-65 text-center bg-white p-20">
                        <img src="/asset2/images/partner.png" alt="">
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="item-area m-t-65 text-center bg-white p-20">
                        <img src="/asset2/images/partner.png" alt="">
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="item-area m-t-65 text-center bg-white p-20">
                        <img src="/asset2/images/partner.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--// client end -->

<!--// contact map -->
<div class="contact-maplocation">
    <div class="container">
        <div class="row row-full">
            <div class="col-md-7 pull-right no-padding col-xs-12">
                <div class="google-map">
                    <div id="map-canvas"></div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-xs-12">
                        <div class="contact m-t-40">
                            <div class="logo">
                                <a href="#"><img src="/asset2/images/logo-2.png" alt=""></a>
                                <div class="contact-element">
                                    <p><i class="fa fa-map-marker contact-icon loacation-icon"></i><span>13/2 Elizabeth Street<br> Melbourne VIC 3000, Australia</span></p>
                                    <p><i class="fa fa-phone contact-icon handphone-icon"></i><span>+ (000) 123 4567 890<br>+ (386) 49 511 868</span><p>
                                    <p><i class="fa fa-envelope contact-icon message-icon"></i><span>dreamhome@gmail.com<br>yourmeail@dreamhome.com</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--// contact map end-->

<!-- footer -->
<div class="footer-section">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="copy-right">
                    <p>Dream Home Premium Theme built by <a href="#">VicTheme.com</a></p>
                </div>
            </div>

            <div class="col-md-6 text-right">
                <ul class="info-list footer-icons">
                    <li>
                        <div>
                            <a href="#"><i class="fa fa-facebook social-icon-link"></i></a>
                            <a href="#"><i class="fa fa-twitter social-icon-link"></i></a>
                            <a href="#"><i class="fa fa-linkedin social-icon-link"></i></a>
                            <a href="#"><i class="fa fa-rss social-icon-link"></i></a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- footer end -->

<!-- == jQuery Librery == -->
<script src="/asset2/js/jquery-1.12.0.min.js"></script>
<!-- == Bootsrap js File == -->
<script src="/asset2/js/bootstrap.min.js"></script>
<!-- == owl carousel js == -->

<script src="/asset2/js/owl.carousel.min.js"></script>
<!-- == map js File == -->
<script src="/asset2/js/gmap3.min.js"></script>
<script src="/asset2/js/map-init.js"></script>
<!-- == map api link == -->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>
<!-- == custom js File == -->
<script src="/asset2/js/jquery.mixitup.min.js"></script>
<!-- == Boostrap Nab Js == -->
<script src="/asset2/js/bootsnav.js"></script>
<script src="/asset2/js/custom.js"></script>
<script type="text/javascript">
    jQuery('#product-grid').mixItUp();
    // owl carousel testimonial
    $(".testimonial-slider").owlCarousel({

        navigation: false, // Show next and prev buttons
        pagination: false,
        slideSpeed: 300,
        paginationSpeed: 400,
        items: 1,
        itemsDesktop: [1024, 1],
        itemsDesktopSmall: [768, 1],
        itemsTablet: [650, 1],
        itemsMobile: 1,
    });
    $("#client").owlCarousel({

        navigation: false, // Show next and prev buttons
        slideSpeed: 300,
        paginationSpeed: 400,
        items: 4,
        pagination: true,
        itemsDesktop: [1199, 4],
        itemsDesktopSmall: [979, 3]
    });
</script>
</body>


<!-- Mirrored from dreamer.victheme.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 11 Jul 2019 07:18:45 GMT -->
</html>
