<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create5cd931a70f46bDocumentationProjectOperationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('documentation_project_operation')) {
            Schema::create('documentation_project_operation', function (Blueprint $table) {
                $table->integer('documentation_id')->unsigned()->nullable();
                $table->foreign('documentation_id', 'fk_p_34988_35458_projecto_5cd931a70f534')->references('id')->on('documentations')->onDelete('cascade');
                $table->integer('project_operation_id')->unsigned()->nullable();
                $table->foreign('project_operation_id', 'fk_p_35458_34988_document_5cd931a70f58a')->references('id')->on('project_operations')->onDelete('cascade');
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documentation_project_operation');
    }
}
