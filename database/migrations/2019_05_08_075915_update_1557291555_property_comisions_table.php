<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1557291555PropertyComisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property_comisions', function (Blueprint $table) {
            
if (!Schema::hasColumn('property_comisions', 'base_amount')) {
                $table->decimal('base_amount', 15, 2)->nullable();
                }
if (!Schema::hasColumn('property_comisions', 'amount')) {
                $table->decimal('amount', 15, 2)->nullable();
                }
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_comisions', function (Blueprint $table) {
            $table->dropColumn('base_amount');
            $table->dropColumn('amount');
            
        });

    }
}
