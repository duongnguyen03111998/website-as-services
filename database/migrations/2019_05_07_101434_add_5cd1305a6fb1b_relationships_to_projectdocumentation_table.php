<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5cd1305a6fb1bRelationshipsToProjectDocumentationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_documentations', function(Blueprint $table) {
            if (!Schema::hasColumn('project_documentations', 'project_id')) {
                $table->integer('project_id')->unsigned()->nullable();
                $table->foreign('project_id', '35264_5cd13059a2f46')->references('id')->on('projects')->onDelete('cascade');
                }
                if (!Schema::hasColumn('project_documentations', 'documentation_id')) {
                $table->integer('documentation_id')->unsigned()->nullable();
                $table->foreign('documentation_id', '35264_5cd13059af46d')->references('id')->on('documentations')->onDelete('cascade');
                }
                if (!Schema::hasColumn('project_documentations', 'created_by_id')) {
                $table->integer('created_by_id')->unsigned()->nullable();
                $table->foreign('created_by_id', '35264_5cd13059bc3f5')->references('id')->on('users')->onDelete('cascade');
                }
                if (!Schema::hasColumn('project_documentations', 'created_by_team_id')) {
                $table->integer('created_by_team_id')->unsigned()->nullable();
                $table->foreign('created_by_team_id', '35264_5cd13059c8ee0')->references('id')->on('teams')->onDelete('cascade');
                }
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_documentations', function(Blueprint $table) {
            
        });
    }
}
