<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5cd26223aa905RelationshipsToPropertyComisionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property_comisions', function(Blueprint $table) {
            if (!Schema::hasColumn('property_comisions', 'property_id')) {
                $table->integer('property_id')->unsigned()->nullable();
                $table->foreign('property_id', '35296_5cd25ea03e949')->references('id')->on('properties')->onDelete('cascade');
                }
                if (!Schema::hasColumn('property_comisions', 'comission_id')) {
                $table->integer('comission_id')->unsigned()->nullable();
                $table->foreign('comission_id', '35296_5cd25ea04ad15')->references('id')->on('comissions')->onDelete('cascade');
                }
                if (!Schema::hasColumn('property_comisions', 'user_id')) {
                $table->integer('user_id')->unsigned()->nullable();
                $table->foreign('user_id', '35296_5cd25ea056a66')->references('id')->on('users')->onDelete('cascade');
                }
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_comisions', function(Blueprint $table) {
            
        });
    }
}
