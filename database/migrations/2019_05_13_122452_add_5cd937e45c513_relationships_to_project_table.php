<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5cd937e45c513RelationshipsToProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function(Blueprint $table) {
            if (!Schema::hasColumn('projects', 'invesloper_id')) {
                $table->integer('invesloper_id')->unsigned()->nullable();
                $table->foreign('invesloper_id', '34992_5ccabcca19ef7')->references('id')->on('inveslopers')->onDelete('cascade');
                }
                if (!Schema::hasColumn('projects', 'created_by_id')) {
                $table->integer('created_by_id')->unsigned()->nullable();
                $table->foreign('created_by_id', '34992_5ccc214b91cde')->references('id')->on('users')->onDelete('cascade');
                }
                if (!Schema::hasColumn('projects', 'created_by_team_id')) {
                $table->integer('created_by_team_id')->unsigned()->nullable();
                $table->foreign('created_by_team_id', '34992_5ccc214b9f94f')->references('id')->on('teams')->onDelete('cascade');
                }
                if (!Schema::hasColumn('projects', 'project_status_id')) {
                $table->integer('project_status_id')->unsigned()->nullable();
                $table->foreign('project_status_id', '34992_5cd9163e9e942')->references('id')->on('project_statuses')->onDelete('cascade');
                }
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects', function(Blueprint $table) {
            
        });
    }
}
