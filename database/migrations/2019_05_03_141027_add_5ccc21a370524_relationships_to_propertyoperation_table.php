<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5ccc21a370524RelationshipsToPropertyOperationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property_operations', function(Blueprint $table) {
            if (!Schema::hasColumn('property_operations', 'property_id')) {
                $table->integer('property_id')->unsigned()->nullable();
                $table->foreign('property_id', '35016_5ccbaa1f082da')->references('id')->on('properties')->onDelete('cascade');
                }
                if (!Schema::hasColumn('property_operations', 'property_status_id')) {
                $table->integer('property_status_id')->unsigned()->nullable();
                $table->foreign('property_status_id', '35016_5ccbaa1f16f5d')->references('id')->on('property_statuses')->onDelete('cascade');
                }
                if (!Schema::hasColumn('property_operations', 'created_by_id')) {
                $table->integer('created_by_id')->unsigned()->nullable();
                $table->foreign('created_by_id', '35016_5ccc21a2caf94')->references('id')->on('users')->onDelete('cascade');
                }
                if (!Schema::hasColumn('property_operations', 'created_by_team_id')) {
                $table->integer('created_by_team_id')->unsigned()->nullable();
                $table->foreign('created_by_team_id', '35016_5ccc21a2d3208')->references('id')->on('teams')->onDelete('cascade');
                }
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_operations', function(Blueprint $table) {
            
        });
    }
}
