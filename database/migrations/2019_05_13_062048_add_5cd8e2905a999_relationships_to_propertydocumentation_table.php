<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5cd8e2905a999RelationshipsToPropertyDocumentationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property_documentations', function(Blueprint $table) {
            if (!Schema::hasColumn('property_documentations', 'property_id')) {
                $table->integer('property_id')->unsigned()->nullable();
                $table->foreign('property_id', '35440_5cd8e28f8312a')->references('id')->on('properties')->onDelete('cascade');
                }
                if (!Schema::hasColumn('property_documentations', 'documentation_id')) {
                $table->integer('documentation_id')->unsigned()->nullable();
                $table->foreign('documentation_id', '35440_5cd8e28f90383')->references('id')->on('documentations')->onDelete('cascade');
                }
                if (!Schema::hasColumn('property_documentations', 'created_by_id')) {
                $table->integer('created_by_id')->unsigned()->nullable();
                $table->foreign('created_by_id', '35440_5cd8e28f9cc44')->references('id')->on('users')->onDelete('cascade');
                }
                if (!Schema::hasColumn('property_documentations', 'created_by_team_id')) {
                $table->integer('created_by_team_id')->unsigned()->nullable();
                $table->foreign('created_by_team_id', '35440_5cd8e28fab431')->references('id')->on('teams')->onDelete('cascade');
                }
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_documentations', function(Blueprint $table) {
            
        });
    }
}
