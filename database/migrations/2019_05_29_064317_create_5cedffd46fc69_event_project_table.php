<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create5cedffd46fc69EventProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('event_project')) {
            Schema::create('event_project', function (Blueprint $table) {
                $table->integer('event_id')->unsigned()->nullable();
                $table->foreign('event_id', 'fk_p_36321_34992_project__5cedffd46fd1c')->references('id')->on('events')->onDelete('cascade');
                $table->integer('project_id')->unsigned()->nullable();
                $table->foreign('project_id', 'fk_p_34992_36321_event_pr_5cedffd46fd5f')->references('id')->on('projects')->onDelete('cascade');
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_project');
    }
}
