<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create5cd0e75ba7dd7DealUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('deal_user')) {
            Schema::create('deal_user', function (Blueprint $table) {
                $table->integer('deal_id')->unsigned()->nullable();
                $table->foreign('deal_id', 'fk_p_35017_34953_user_dea_5cd0e75ba7eaa')->references('id')->on('deals')->onDelete('cascade');
                $table->integer('user_id')->unsigned()->nullable();
                $table->foreign('user_id', 'fk_p_34953_35017_deal_use_5cd0e75ba7eee')->references('id')->on('users')->onDelete('cascade');
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deal_user');
    }
}
