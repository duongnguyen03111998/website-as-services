<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5ccbaa1fb768bRelationshipsToPropertyOperationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property_operations', function(Blueprint $table) {
            if (!Schema::hasColumn('property_operations', 'property_id')) {
                $table->integer('property_id')->unsigned()->nullable();
                $table->foreign('property_id', '35016_5ccbaa1f082da')->references('id')->on('properties')->onDelete('cascade');
                }
                if (!Schema::hasColumn('property_operations', 'user_id')) {
                $table->integer('user_id')->unsigned()->nullable();
                $table->foreign('user_id', '35016_5ccbaa1f0f84a')->references('id')->on('users')->onDelete('cascade');
                }
                if (!Schema::hasColumn('property_operations', 'property_status_id')) {
                $table->integer('property_status_id')->unsigned()->nullable();
                $table->foreign('property_status_id', '35016_5ccbaa1f16f5d')->references('id')->on('property_statuses')->onDelete('cascade');
                }
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_operations', function(Blueprint $table) {
            
        });
    }
}
