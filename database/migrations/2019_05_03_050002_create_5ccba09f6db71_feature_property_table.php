<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create5ccba09f6db71FeaturePropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('feature_property')) {
            Schema::create('feature_property', function (Blueprint $table) {
                $table->integer('feature_id')->unsigned()->nullable();
                $table->foreign('feature_id', 'fk_p_34960_35008_property_5ccba09f6dc1e')->references('id')->on('features')->onDelete('cascade');
                $table->integer('property_id')->unsigned()->nullable();
                $table->foreign('property_id', 'fk_p_35008_34960_feature__5ccba09f6dc63')->references('id')->on('properties')->onDelete('cascade');
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feature_property');
    }
}
