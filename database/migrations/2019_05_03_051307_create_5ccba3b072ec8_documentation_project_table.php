<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create5ccba3b072ec8DocumentationProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('documentation_project')) {
            Schema::create('documentation_project', function (Blueprint $table) {
                $table->integer('documentation_id')->unsigned()->nullable();
                $table->foreign('documentation_id', 'fk_p_34988_34992_project__5ccba3b072f7e')->references('id')->on('documentations')->onDelete('cascade');
                $table->integer('project_id')->unsigned()->nullable();
                $table->foreign('project_id', 'fk_p_34992_34988_document_5ccba3b072fc7')->references('id')->on('projects')->onDelete('cascade');
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documentation_project');
    }
}
