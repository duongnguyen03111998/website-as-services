<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1556881826PropertyOperationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property_operations', function (Blueprint $table) {
            if(Schema::hasColumn('property_operations', 'user_id')) {
                $table->dropForeign('35016_5ccbaa1f0f84a');
                $table->dropIndex('35016_5ccbaa1f0f84a');
                $table->dropColumn('user_id');
            }
            
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_operations', function (Blueprint $table) {
                        
        });

    }
}
