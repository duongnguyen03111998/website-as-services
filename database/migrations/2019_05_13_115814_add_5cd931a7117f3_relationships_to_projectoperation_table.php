<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5cd931a7117f3RelationshipsToProjectOperationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_operations', function(Blueprint $table) {
            if (!Schema::hasColumn('project_operations', 'project_id')) {
                $table->integer('project_id')->unsigned()->nullable();
                $table->foreign('project_id', '35458_5cd931a63eb79')->references('id')->on('projects')->onDelete('cascade');
                }
                if (!Schema::hasColumn('project_operations', 'project_status_id')) {
                $table->integer('project_status_id')->unsigned()->nullable();
                $table->foreign('project_status_id', '35458_5cd931a64c14b')->references('id')->on('project_stauses')->onDelete('cascade');
                }
                if (!Schema::hasColumn('project_operations', 'created_by_id')) {
                $table->integer('created_by_id')->unsigned()->nullable();
                $table->foreign('created_by_id', '35458_5cd931a6597e0')->references('id')->on('users')->onDelete('cascade');
                }
                if (!Schema::hasColumn('project_operations', 'created_by_team_id')) {
                $table->integer('created_by_team_id')->unsigned()->nullable();
                $table->foreign('created_by_team_id', '35458_5cd931a666c52')->references('id')->on('teams')->onDelete('cascade');
                }
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_operations', function(Blueprint $table) {
            
        });
    }
}
