<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create5ccbaa1fb5a92DocumentationPropertyOperationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('documentation_property_operation')) {
            Schema::create('documentation_property_operation', function (Blueprint $table) {
                $table->integer('documentation_id')->unsigned()->nullable();
                $table->foreign('documentation_id', 'fk_p_34988_35016_property_5ccbaa1fb5b59')->references('id')->on('documentations')->onDelete('cascade');
                $table->integer('property_operation_id')->unsigned()->nullable();
                $table->foreign('property_operation_id', 'fk_p_35016_34988_document_5ccbaa1fb5bae')->references('id')->on('property_operations')->onDelete('cascade');
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documentation_property_operation');
    }
}
