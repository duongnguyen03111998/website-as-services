<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1560571823ProjectDocumentationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_documentations', function (Blueprint $table) {
            
if (!Schema::hasColumn('project_documentations', 'content')) {
                $table->text('content')->nullable();
                }
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_documentations', function (Blueprint $table) {
            $table->dropColumn('content');
            
        });

    }
}
