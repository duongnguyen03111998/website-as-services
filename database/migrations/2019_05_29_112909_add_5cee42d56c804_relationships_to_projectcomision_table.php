<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5cee42d56c804RelationshipsToProjectComisionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_comisions', function(Blueprint $table) {
            if (!Schema::hasColumn('project_comisions', 'project_id')) {
                $table->integer('project_id')->unsigned()->nullable();
                $table->foreign('project_id', '36341_5cee42d48cf69')->references('id')->on('projects')->onDelete('cascade');
                }
                if (!Schema::hasColumn('project_comisions', 'comission_id')) {
                $table->integer('comission_id')->unsigned()->nullable();
                $table->foreign('comission_id', '36341_5cee42d49cbea')->references('id')->on('comissions')->onDelete('cascade');
                }
                if (!Schema::hasColumn('project_comisions', 'user_id')) {
                $table->integer('user_id')->unsigned()->nullable();
                $table->foreign('user_id', '36341_5cee42d4ac924')->references('id')->on('users')->onDelete('cascade');
                }
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_comisions', function(Blueprint $table) {
            
        });
    }
}
