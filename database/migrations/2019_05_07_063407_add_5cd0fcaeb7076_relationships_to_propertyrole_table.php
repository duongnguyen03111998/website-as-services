<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5cd0fcaeb7076RelationshipsToPropertyRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property_roles', function(Blueprint $table) {
            if (!Schema::hasColumn('property_roles', 'user_id')) {
                $table->integer('user_id')->unsigned()->nullable();
                $table->foreign('user_id', '35261_5cd0fcae0e3fc')->references('id')->on('users')->onDelete('cascade');
                }
                if (!Schema::hasColumn('property_roles', 'role_id')) {
                $table->integer('role_id')->unsigned()->nullable();
                $table->foreign('role_id', '35261_5cd0fcae1a4c6')->references('id')->on('role_in_projects')->onDelete('cascade');
                }
                if (!Schema::hasColumn('property_roles', 'property_id')) {
                $table->integer('property_id')->unsigned()->nullable();
                $table->foreign('property_id', '35261_5cd0fcae26518')->references('id')->on('properties')->onDelete('cascade');
                }
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_roles', function(Blueprint $table) {
            
        });
    }
}
