<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Drop5cd9347ecca1b5cd9347ecafb1DocumentationProjectOperationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('documentation_project_operation');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(! Schema::hasTable('documentation_project_operation')) {
            Schema::create('documentation_project_operation', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('documentation_id')->unsigned()->nullable();
            $table->foreign('documentation_id', 'fk_p_34988_35458_projecto_5cd931a630c50')->references('id')->on('documentations');
                $table->integer('project_operation_id')->unsigned()->nullable();
            $table->foreign('project_operation_id', 'fk_p_35458_34988_document_5cd931a63156c')->references('id')->on('project_operations');
                
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }
}
