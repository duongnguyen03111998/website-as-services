<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProjectOldStatusIdToProjectOperationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_operations', function (Blueprint $table) {
            if (!Schema::hasColumn('project_operations','project_old_status_id')) {
                $table->integer('project_old_status_id')->nullable();
            }

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_operations', function (Blueprint $table) {
            //
        });
    }
}
