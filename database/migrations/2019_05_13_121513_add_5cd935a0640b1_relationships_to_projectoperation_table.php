<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5cd935a0640b1RelationshipsToProjectOperationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_operations', function(Blueprint $table) {
            if (!Schema::hasColumn('project_operations', 'project_id')) {
                $table->integer('project_id')->unsigned()->nullable();
                $table->foreign('project_id', '35460_5cd9359fb09d9')->references('id')->on('projects')->onDelete('cascade');
                }
                if (!Schema::hasColumn('project_operations', 'project_status_id')) {
                $table->integer('project_status_id')->unsigned()->nullable();
                $table->foreign('project_status_id', '35460_5cd9359fb8748')->references('id')->on('project_statuses')->onDelete('cascade');
                }
                if (!Schema::hasColumn('project_operations', 'created_by_id')) {
                $table->integer('created_by_id')->unsigned()->nullable();
                $table->foreign('created_by_id', '35460_5cd9359fc0c84')->references('id')->on('users')->onDelete('cascade');
                }
                if (!Schema::hasColumn('project_operations', 'created_by_team_id')) {
                $table->integer('created_by_team_id')->unsigned()->nullable();
                $table->foreign('created_by_team_id', '35460_5cd9359fc818a')->references('id')->on('teams')->onDelete('cascade');
                }
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_operations', function(Blueprint $table) {
            
        });
    }
}
