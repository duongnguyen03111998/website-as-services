<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update1556790837ProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function (Blueprint $table) {
            
if (!Schema::hasColumn('projects', 'project_area')) {
                $table->string('project_area')->nullable();
                }
if (!Schema::hasColumn('projects', 'block')) {
                $table->integer('block')->nullable();
                }
if (!Schema::hasColumn('projects', 'floor')) {
                $table->integer('floor')->nullable();
                }
if (!Schema::hasColumn('projects', 'unit')) {
                $table->integer('unit')->nullable();
                }
if (!Schema::hasColumn('projects', 'from_price')) {
                $table->decimal('from_price', 15, 2)->nullable();
                }
if (!Schema::hasColumn('projects', 'to_price')) {
                $table->decimal('to_price', 15, 2)->nullable();
                }
if (!Schema::hasColumn('projects', 'day_of_sale')) {
                $table->date('day_of_sale')->nullable();
                }
if (!Schema::hasColumn('projects', 'description')) {
                $table->text('description')->nullable();
                }
if (!Schema::hasColumn('projects', 'address')) {
                $table->string('address')->nullable();
                }
if (!Schema::hasColumn('projects', 'latitude')) {
                $table->double('latitude', 15, 2)->nullable();
                }
if (!Schema::hasColumn('projects', 'longitude')) {
                $table->double('longitude', 15, 2)->nullable();
                }
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->dropColumn('project_area');
            $table->dropColumn('block');
            $table->dropColumn('floor');
            $table->dropColumn('unit');
            $table->dropColumn('from_price');
            $table->dropColumn('to_price');
            $table->dropColumn('day_of_sale');
            $table->dropColumn('description');
            $table->dropColumn('address');
            $table->dropColumn('latitude');
            $table->dropColumn('longitude');
            
        });

    }
}
