<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create5ccba7cd78c78PropertyUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('property_user')) {
            Schema::create('property_user', function (Blueprint $table) {
                $table->integer('property_id')->unsigned()->nullable();
                $table->foreign('property_id', 'fk_p_35008_34953_user_pro_5ccba7cd78d2e')->references('id')->on('properties')->onDelete('cascade');
                $table->integer('user_id')->unsigned()->nullable();
                $table->foreign('user_id', 'fk_p_34953_35008_property_5ccba7cd78d72')->references('id')->on('users')->onDelete('cascade');
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_user');
    }
}
