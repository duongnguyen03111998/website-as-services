<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInitBookingNumberToProjectUnitTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_unit_types', function (Blueprint $table) {
            if(!Schema::hasColumn('project_unit_types', 'init_booking_number')) {
                $table->integer('init_booking_number')->nullable();
            }

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_unit_types', function (Blueprint $table) {
            //
        });
    }
}
