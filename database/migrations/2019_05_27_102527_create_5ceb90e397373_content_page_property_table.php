<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create5ceb90e397373ContentPagePropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('content_page_property')) {
            Schema::create('content_page_property', function (Blueprint $table) {
                $table->integer('content_page_id')->unsigned()->nullable();
                $table->foreign('content_page_id', 'fk_p_36163_35008_property_5ceb90e397462')->references('id')->on('content_pages')->onDelete('cascade');
                $table->integer('property_id')->unsigned()->nullable();
                $table->foreign('property_id', 'fk_p_35008_36163_contentp_5ceb90e3974dc')->references('id')->on('properties')->onDelete('cascade');
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_page_property');
    }
}
