<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5ccace335d3e8RelationshipsToProjectRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_roles', function(Blueprint $table) {
            if (!Schema::hasColumn('project_roles', 'project_id')) {
                $table->integer('project_id')->unsigned()->nullable();
                $table->foreign('project_id', '34995_5ccace32b5793')->references('id')->on('projects')->onDelete('cascade');
                }
                if (!Schema::hasColumn('project_roles', 'role_id')) {
                $table->integer('role_id')->unsigned()->nullable();
                $table->foreign('role_id', '34995_5ccace32bbc1d')->references('id')->on('role_in_projects')->onDelete('cascade');
                }
                if (!Schema::hasColumn('project_roles', 'user_id')) {
                $table->integer('user_id')->unsigned()->nullable();
                $table->foreign('user_id', '34995_5ccace32c1d88')->references('id')->on('users')->onDelete('cascade');
                }
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_roles', function(Blueprint $table) {
            
        });
    }
}
