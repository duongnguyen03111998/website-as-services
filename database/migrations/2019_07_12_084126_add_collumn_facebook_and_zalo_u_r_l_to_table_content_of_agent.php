<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCollumnFacebookAndZaloURLToTableContentOfAgent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('content_of_agents', function (Blueprint $table) {
            $table->string('facebook_url');
            $table->string('zalo_url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('content_of_agents', function (Blueprint $table) {
            //
        });
    }
}
