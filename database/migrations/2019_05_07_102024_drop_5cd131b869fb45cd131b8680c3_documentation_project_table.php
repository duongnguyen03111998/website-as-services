<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Drop5cd131b869fb45cd131b8680c3DocumentationProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('documentation_project');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(! Schema::hasTable('documentation_project')) {
            Schema::create('documentation_project', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('documentation_id')->unsigned()->nullable();
            $table->foreign('documentation_id', 'fk_p_34988_34992_project__5ccba3b0714aa')->references('id')->on('documentations');
                $table->integer('project_id')->unsigned()->nullable();
            $table->foreign('project_id', 'fk_p_34992_34988_document_5ccba3b07202f')->references('id')->on('projects');
                
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }
}
