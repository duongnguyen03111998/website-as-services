<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOldStatusToPropertyOperation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('property_operations', function (Blueprint $table) {
            if (!Schema::hasColumn('property_operations','property_old_status_id')) {
                $table->integer('property_old_status_id')->nullable();
            }



        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_operations', function (Blueprint $table) {
            //
        });
    }
}
