<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5cf60f2b98caaRelationshipsToProjectUnitTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_unit_types', function(Blueprint $table) {
            if (!Schema::hasColumn('project_unit_types', 'project_id')) {
                $table->integer('project_id')->unsigned()->nullable();
                $table->foreign('project_id', '36649_5cf60f2aad206')->references('id')->on('projects')->onDelete('cascade');
                }
                if (!Schema::hasColumn('project_unit_types', 'unit_type_id')) {
                $table->integer('unit_type_id')->unsigned()->nullable();
                $table->foreign('unit_type_id', '36649_5cf60f2abd711')->references('id')->on('unit_types')->onDelete('cascade');
                }
                if (!Schema::hasColumn('project_unit_types', 'created_by_id')) {
                $table->integer('created_by_id')->unsigned()->nullable();
                $table->foreign('created_by_id', '36649_5cf60f2acd9a1')->references('id')->on('users')->onDelete('cascade');
                }
                if (!Schema::hasColumn('project_unit_types', 'created_by_team_id')) {
                $table->integer('created_by_team_id')->unsigned()->nullable();
                $table->foreign('created_by_team_id', '36649_5cf60f2add05d')->references('id')->on('teams')->onDelete('cascade');
                }
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_unit_types', function(Blueprint $table) {
            
        });
    }
}
