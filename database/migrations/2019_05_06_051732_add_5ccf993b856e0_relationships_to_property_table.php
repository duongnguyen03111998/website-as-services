<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5ccf993b856e0RelationshipsToPropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('properties', function(Blueprint $table) {
            if (!Schema::hasColumn('properties', 'transaction_type_id')) {
                $table->integer('transaction_type_id')->unsigned()->nullable();
                $table->foreign('transaction_type_id', '35008_5ccba09e76057')->references('id')->on('transaction_types')->onDelete('cascade');
                }
                if (!Schema::hasColumn('properties', 'category_id')) {
                $table->integer('category_id')->unsigned()->nullable();
                $table->foreign('category_id', '35008_5ccba09e7f03a')->references('id')->on('categories')->onDelete('cascade');
                }
                if (!Schema::hasColumn('properties', 'project_id')) {
                $table->integer('project_id')->unsigned()->nullable();
                $table->foreign('project_id', '35008_5ccba09e8806c')->references('id')->on('projects')->onDelete('cascade');
                }
                if (!Schema::hasColumn('properties', 'district_id')) {
                $table->integer('district_id')->unsigned()->nullable();
                $table->foreign('district_id', '35008_5ccba09e90fb9')->references('id')->on('districts')->onDelete('cascade');
                }
                if (!Schema::hasColumn('properties', 'house_face_id')) {
                $table->integer('house_face_id')->unsigned()->nullable();
                $table->foreign('house_face_id', '35008_5ccba1999a796')->references('id')->on('house_faces')->onDelete('cascade');
                }
                if (!Schema::hasColumn('properties', 'home_form_id')) {
                $table->integer('home_form_id')->unsigned()->nullable();
                $table->foreign('home_form_id', '35008_5ccba34c20068')->references('id')->on('home_forms')->onDelete('cascade');
                }
                if (!Schema::hasColumn('properties', 'property_status_id')) {
                $table->integer('property_status_id')->unsigned()->nullable();
                $table->foreign('property_status_id', '35008_5ccba374e20e3')->references('id')->on('property_statuses')->onDelete('cascade');
                }
                if (!Schema::hasColumn('properties', 'user_id')) {
                $table->integer('user_id')->unsigned()->nullable();
                $table->foreign('user_id', '35008_5ccba7a142c58')->references('id')->on('users')->onDelete('cascade');
                }
                if (!Schema::hasColumn('properties', 'created_by_id')) {
                $table->integer('created_by_id')->unsigned()->nullable();
                $table->foreign('created_by_id', '35008_5ccc2169bce56')->references('id')->on('users')->onDelete('cascade');
                }
                if (!Schema::hasColumn('properties', 'created_by_team_id')) {
                $table->integer('created_by_team_id')->unsigned()->nullable();
                $table->foreign('created_by_team_id', '35008_5ccc2169c909f')->references('id')->on('teams')->onDelete('cascade');
                }
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties', function(Blueprint $table) {
            
        });
    }
}
