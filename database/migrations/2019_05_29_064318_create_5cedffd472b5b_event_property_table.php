<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create5cedffd472b5bEventPropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(! Schema::hasTable('event_property')) {
            Schema::create('event_property', function (Blueprint $table) {
                $table->integer('event_id')->unsigned()->nullable();
                $table->foreign('event_id', 'fk_p_36321_35008_property_5cedffd472c03')->references('id')->on('events')->onDelete('cascade');
                $table->integer('property_id')->unsigned()->nullable();
                $table->foreign('property_id', 'fk_p_35008_36321_event_pr_5cedffd472c4a')->references('id')->on('properties')->onDelete('cascade');
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_property');
    }
}
