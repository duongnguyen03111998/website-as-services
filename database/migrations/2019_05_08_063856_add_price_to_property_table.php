<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPriceToPropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('properties', function (Blueprint $table) {
            if (!Schema::hasColumn('properties','price')) {
                $table->decimal('price', 20, 2)->nullable();
            }
            if (!Schema::hasColumn('properties','comision_rate')) {
                $table->decimal('comision_rate', 20, 2)->nullable();
            }
            if (!Schema::hasColumn('properties','comision_amount')) {
                $table->decimal('comision_amount', 20, 2)->nullable();
            }


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties', function (Blueprint $table) {
            //
        });
    }
}
