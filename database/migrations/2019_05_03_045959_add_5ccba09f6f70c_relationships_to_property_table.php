<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add5ccba09f6f70cRelationshipsToPropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('properties', function(Blueprint $table) {
            if (!Schema::hasColumn('properties', 'transaction_type_id')) {
                $table->integer('transaction_type_id')->unsigned()->nullable();
                $table->foreign('transaction_type_id', '35008_5ccba09e76057')->references('id')->on('transaction_types')->onDelete('cascade');
                }
                if (!Schema::hasColumn('properties', 'category_id')) {
                $table->integer('category_id')->unsigned()->nullable();
                $table->foreign('category_id', '35008_5ccba09e7f03a')->references('id')->on('categories')->onDelete('cascade');
                }
                if (!Schema::hasColumn('properties', 'project_id')) {
                $table->integer('project_id')->unsigned()->nullable();
                $table->foreign('project_id', '35008_5ccba09e8806c')->references('id')->on('projects')->onDelete('cascade');
                }
                if (!Schema::hasColumn('properties', 'district_id')) {
                $table->integer('district_id')->unsigned()->nullable();
                $table->foreign('district_id', '35008_5ccba09e90fb9')->references('id')->on('districts')->onDelete('cascade');
                }
                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties', function(Blueprint $table) {
            
        });
    }
}
