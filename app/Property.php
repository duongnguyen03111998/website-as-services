<?php
namespace App;

use App\Rules\CheckApprovePropertyStatusPermissionRule;
use App\Rules\CheckSubmitPropertyStatusPermissionRule;
use App\Rules\CheckUpdatePropertyStatusPermissionRule;
use App\Traits\FilterByAgent;
use App\Traits\FilterByTeam;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\FilterByUser;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Class Property
 *
 * @package App
 * @property string $title
 * @property string $slug
 * @property string $transaction_type
 * @property string $category
 * @property string $project
 * @property string $district
 * @property string $area
 * @property integer $year
 * @property string $floor
 * @property integer $rooms
 * @property integer $bedroom
 * @property integer $bathroom
 * @property string $unit_number
 * @property string $house_face
 * @property text $description
 * @property string $code_name
 * @property string $home_form
 * @property string $property_status
 * @property string $user
 * @property string $created_by
 * @property string $created_by_team
 * @property string $address
 * @property double $latitude
 * @property double $longitude
 * @property string $block
*/
class Property extends Model implements HasMedia
{
    use SoftDeletes, FilterByTeam,FilterByAgent, HasMediaTrait;


    protected $fillable = ['comision_amount','referral_id','price','comision_rate' ,'title', 'slug', 'area', 'year', 'floor', 'rooms', 'bedroom', 'bathroom', 'unit_number', 'description', 'code_name', 'address', 'latitude', 'longitude', 'transaction_type_id', 'category_id', 'project_id', 'district_id', 'house_face_id', 'home_form_id', 'property_status_id', 'created_by_id', 'created_by_team_id', 'block_id'];
    protected $appends = ['url', 'photos','photo_urls','thumb_urls', 'photos_link', 'uploaded_photos','price_text'];
    protected $with = ['media'];

    public function registerAllMediaConversions()
    {


        $this->addMediaConversion('watermark')
            ->watermark(storage_path().'/watermark/logo.png')
            ->setManipulations(['w' => 1024, 'h' => 1024])
            ->watermarkPosition(Manipulations::POSITION_BOTTOM_RIGHT)
            ->watermarkPadding(10, 10, Manipulations::UNIT_PERCENT)
            ->watermarkOpacity(0)
            ->performOnCollections('photos')
            ->nonQueued();

        $this->addMediaConversion('thumb')
            ->watermark(storage_path().'/watermark/logo.png')
            ->setManipulations(['w' => 512, 'h' => 512])
            ->watermarkPosition(Manipulations::POSITION_BOTTOM_RIGHT)
            ->watermarkPadding(10, 10, Manipulations::UNIT_PERCENT)
            ->watermarkOpacity(0)
            ->performOnCollections('photos')
            ->nonQueued();


    }

    public static function boot()
    {
        parent::boot();

        Property::observe(new \App\Observers\UserActionsObserver);
        Property::observe(new \App\Observers\PropertyObserver());

        static::creating(function ($model) {
          //  $model->property_status_id = 1;
            $model->slug = str_slug($model->title);
        });
        static::saving(function ($model) {
            $model->slug = str_slug($model->title);
        });


    }

    protected $casts = [
        'price' => 'float',
        'comision_rate' => 'float',
        'comision_amount' => 'float'
    ];


    public static function storeValidation($request)
    {
        return [
            'title' => 'max:191|required',
            'slug' => 'max:191',
            'transaction_type_id' => 'integer|exists:transaction_types,id|max:4294967295|required',
            'category_id' => 'integer|exists:categories,id|max:4294967295|required',
            'project_id' => 'integer|exists:projects,id|max:4294967295|nullable',
            'district_id' => 'integer|exists:districts,id|max:4294967295|required',
            'features' => 'array|nullable',
            'features.*' => 'integer|exists:features,id|max:4294967295|nullable',
            'area' => 'max:191|nullable',
            'year' => 'integer|max:2147483647|nullable',
            'floor' => 'max:191|nullable',
            'rooms' => 'integer|max:2147483647|nullable',
            'bedroom' => 'integer|max:2147483647|nullable',
            'bathroom' => 'integer|max:2147483647|nullable',
            'unit_number' => 'max:191|nullable',
            'house_face_id' => 'integer|exists:house_faces,id|max:4294967295|nullable',
            'description' => 'max:65535|nullable',
            'code_name' => 'max:191|nullable',
            'home_form_id' => 'integer|exists:home_forms,id|max:4294967295|nullable',
            'agents' => 'array|nullable',
            'agents.*' => 'integer|exists:users,id|max:4294967295|nullable',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable',
            'created_by_team_id' => 'integer|exists:teams,id|max:4294967295|nullable',
            'address' => 'max:191|nullable',
            'latitude' => 'numeric|nullable',
            'longitude' => 'numeric|nullable',
            'block_id' => 'integer|exists:blocks,id|max:4294967295|nullable',
            'price'  => 'numeric|required'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'title' => 'max:191|required',
            'transaction_type_id' => 'integer|exists:transaction_types,id|max:4294967295|required',
            'category_id' => 'integer|exists:categories,id|max:4294967295|required',
            'project_id' => 'integer|exists:projects,id|max:4294967295|nullable',
            'district_id' => 'integer|exists:districts,id|max:4294967295|required',
            'photos' => 'sometimes',
            'photos.*' => 'file|image|nullable',
            'features' => 'array|nullable',
            'features.*' => 'integer|exists:features,id|max:4294967295|nullable',
            'area' => 'max:191|nullable',
            'year' => 'integer|max:2147483647|nullable',
            'floor' => 'max:191|nullable',
            'rooms' => 'integer|max:2147483647|nullable',
            'bedroom' => 'integer|max:2147483647|nullable',
            'bathroom' => 'integer|max:2147483647|nullable',
            'unit_number' => 'max:191|nullable',
            'house_face_id' => 'integer|exists:house_faces,id|max:4294967295|nullable',
            'description' => 'max:65535|nullable',
            'code_name' => 'max:191|nullable',
            'home_form_id' => 'integer|exists:home_forms,id|max:4294967295|nullable',
            'property_status_id' => new CheckUpdatePropertyStatusPermissionRule($request->id),
            'agents' => 'array|nullable',
            'agents.*' => 'integer|exists:users,id|max:4294967295|nullable',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable',
            'created_by_team_id' => 'integer|exists:teams,id|max:4294967295|nullable',
            'address' => 'max:191|nullable',
            'latitude' => 'numeric|nullable',
            'longitude' => 'numeric|nullable',
            'block_id' => 'integer|exists:blocks,id|max:4294967295|nullable'
        ];
    }


    public static function submitValidation($request)
    {

        return [
            'property_status_id' => ['required',new CheckSubmitPropertyStatusPermissionRule($request->id)]
        ];
    }

    public static function approveValidation($request)
    {
        return [
            'property_status_id' => ['required',new CheckApprovePropertyStatusPermissionRule($request->id)]
        ];
    }




    public function getPriceTextAttribute()
    {

        return convert_number_to_words($this->price);

    }


    public function getPhotosAttribute()
    {

        return [];

    }

    public function getThumbUrlsAttribute()
    {
        $photos = $this->getMedia('photos');
        if (! count($photos)) {
            return null;
        }
        $files = [];
        foreach ($photos as $file) {
            $file->url =  $file->getUrl('thumb');
            $files[] =$file;
        }
        return $files;

    }

    public function getComisionAmountAttribute()
    {


        $user = Auth::user();
        $roles =$user->role->pluck('id')->toArray() ;
        if (in_array(6, $roles) and count($roles) == 1) {
            return null;
        }else{
            return floatval($this->attributes['comision_amount']);

        }

    }


    public function getPhotoUrlsAttribute()
    {
        $photos = $this->getMedia('photos');
        if (! count($photos)) {
            return null;
        }
        $files = [];
        foreach ($photos as $file) {
            $file->url =  ($file->getUrl('watermark'));
            $files[] =$file;
        }
        return $files;

    }

    public function getUrlAttribute()
    {
        return  url("/admin/properties/".$this->id);

    }

    public function getUploadedPhotosAttribute()
    {
        return $this->getMedia('photos')->keyBy('id');
    }

    /**
     * @return string
     */
    public function getPhotosLinkAttribute()
    {
        $photos = $this->getMedia('photos');
        if (! count($photos)) {
            return null;
        }
        $html = [];
        foreach ($photos as $file) {

            $html[] = '<a href="' . $file->getUrl('watermark') . '" target="_blank">' . $file->file_name . '</a>';
        }

        return implode('<br/>', $html);
    }
    
    public function transaction_type()
    {
        return $this->belongsTo(TransactionType::class, 'transaction_type_id')->withTrashed();
    }
    
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id')->withTrashed();
    }
    
    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id')->withTrashed();
    }
    
    public function district()
    {
        return $this->belongsTo(District::class, 'district_id')->withTrashed();
    }
    
    public function features()
    {
        return $this->belongsToMany(Feature::class, 'feature_property')->withTrashed();
    }
    
    public function house_face()
    {
        return $this->belongsTo(HouseFace::class, 'house_face_id')->withTrashed();
    }
    
    public function home_form()
    {
        return $this->belongsTo(HomeForm::class, 'home_form_id')->withTrashed();
    }
    
    public function property_status()
    {
        return $this->belongsTo(PropertyStatus::class, 'property_status_id')->withTrashed();
    }
    

    
    public function agents()
    {
        return $this->belongsToMany(User::class, 'property_user');
    }
    
    public function created_by()
    {
        return $this->belongsTo(User::class, 'created_by_id');
    }


    public function referral()
    {
        return $this->belongsTo(User::class, 'referral_id');
    }


    public function created_by_team()
    {
        return $this->belongsTo(Team::class, 'created_by_team_id');
    }
    
    public function block()
    {
        return $this->belongsTo(Block::class, 'block_id')->withTrashed();
    }

    public function commissions()
    {
        return $this->hasMany(PropertyComision::class, 'property_id');
    }

}
