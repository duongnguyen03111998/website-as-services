<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Log;

trait FilterByAgent
{
    protected static function bootFilterByAgent()
    {
        if (! app()->runningInConsole() && auth('api')->check()) {
            $user        = auth('api')->user();
            $table       = (new static)->getTable();
            $all_role_id = config('quickadmin.can_see_all_records_role_id');
            if (! in_array($all_role_id, $user->role->pluck('id')->toArray())) {

                if($table !== 'users'){
                    static::creating(function ($model) use ($user) {
                        $model->created_by_id      = $user->id;
                        $model->created_by_team_id = $user->team_id;
                    });

                }
                $roles =$user->role->pluck('id')->toArray() ;

                    if (in_array(6, $roles) and count($roles) == 1) {

                        if ($table === 'content_pages') {
                            static::addGlobalScope('status', function (Builder $builder) use ($user) {
                                $builder->whereIn('status', [4]);
                            });
                        }

                        if ($table === 'properties') {
                            static::addGlobalScope('property_status_id', function (Builder $builder) use ($user) {
                                $builder->where('property_status_id', 3);
                            });
                        }
                        if ($table === 'projects') {
                            static::addGlobalScope('project_status_id', function (Builder $builder) use ($user) {
                                $builder->where('project_status_id', 3);
                            });
                        }
                        if ($table === 'events') {
                            static::addGlobalScope('status', function (Builder $builder) use ($user) {
                                $builder->whereIn('status', [4]);
                            });
                        }

                    }


            }

        }
    }
}
