<?php
namespace App;

use App\Traits\FilterByUser;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Notifications\ResetPassword;
use Hash;
use App\Traits\FilterByTeam;
use Illuminate\Support\Facades\Log;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Laravel\Passport\HasApiTokens;

/**
 * Class User
 *
 * @package App
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
 * @property string $team
 * @property tinyInteger $approved
 * @property string $avatar
*/
class User extends Authenticatable implements MustVerifyEmail,HasMedia
{
    use Notifiable;
    use FilterByTeam, HasMediaTrait, HasApiTokens;

    
    protected $fillable = ['last_change_password_at','invited_by_id','invitation_code','name', 'email', 'password','phone', 'remember_token', 'approved', 'team_id','migrate_id','midesk_id','avatar','facebook','google','otp_verified_at'];
    protected $hidden = ['password', 'remember_token'];
    protected $appends = ['avatar', 'avatar_link','avatar_url','referral_url'];
    protected $with = ['media'];

    public static function storeValidation($request)
    {
        return [
            'name' => 'max:191|required',
            'email' => 'email|max:191|required|unique:users,email',
            'phone' => 'max:191|required|unique:users,phone',
            'password' => [
                'required',
                'min:6',             // must be at least 10 characters in length
            ],
            'role' => 'array|required',
            'role.*' => 'integer|exists:roles,id|max:4294967295|required',
            'remember_token' => 'max:191|nullable',
            'team_id' => 'integer|exists:teams,id|max:4294967295|nullable',
            'approved' => 'boolean|nullable',
            'avatar' => 'file|image|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'name' => 'max:191|nullable',
            'email' => 'email|max:191|nullable|unique:users,email,'.$request->route('user'),
            'password' => '',
            'phone' => 'max:191|sometimes|unique:users,phone,'.$request->route('user'),
            'role' => 'array|nullable',
            'role.*' => 'integer|exists:roles,id|max:4294967295|nullable',
            'remember_token' => 'max:191|nullable',
            'team_id' => 'integer|exists:teams,id|max:4294967295|nullable',
            'approved' => 'boolean|nullable',
            'avatar' => 'nullable'
        ];
    }

    
    

    public static function boot()
    {
        parent::boot();

        User::observe(new \App\Observers\UserActionsObserver);
        User::observe(new \App\Observers\UserObserver);
    }
    
    /**
     * Hash password
     * @param $input
     */
    public function setPasswordAttribute($input)
    {
        if ($input) {
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
        }
    }

    public function getAvatarAttribute()
    {
        return $this->getFirstMedia('avatar');
    }

    /**
     * @return string
     */
    public function getAvatarLinkAttribute()
    {
        $file = $this->getFirstMedia('avatar');
        if (! $file) {
            return null;
        }

        return '<a href="' . $file->getUrl() . '" target="_blank">' . $file->file_name . '</a>';
    }

    /**
     * @return string
     */
    public function getAvatarUrlAttribute()
    {
        $file = $this->getFirstMedia('avatar');
        if (! $file) {
            return null;
        }

        return $file->getUrl();
    }
    
    public function role()
    {
        return $this->belongsToMany(Role::class, 'role_user');
    }

    public function profile()
    {
        return $this->hasOne(Profile::class, 'user_id');
    }

    public function push_devices()
    {
        return $this->hasMany(PushDevice::class, 'user_id');
    }



    public function invited_by()
    {
        return $this->belongsTo(User::class, 'invited_by_id');
    }



    public function team()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }


    public function findForPassport($identifier)
    {
        return $this->orWhere('email', $identifier)->orWhere('phone', $identifier)->first();
    }

        public function sendPasswordResetNotification($token)
    {
       $this->notify(new ResetPassword($token));
    }





    public function deals()
    {
        return $this->hasMany(Deal::class, 'created_by_id');
    }

    public function support_deals()
    {
        return $this->hasMany(Deal::class, 'supporter_id');
    }

    public function support_requests()
    {
        return $this->hasMany(Requests::class, 'supporter_id');
    }

    public function getReferralUrlAttribute()
    {
        return env('APP_AGENT_URL').'/referral?code='.$this->phone;
    }


    /**
     * Route notifications for the FCM channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return string
     */
    public function routeNotificationForFcm()
    {
        return $this->push_devices->pluck('token')->toArray();
    }
}
