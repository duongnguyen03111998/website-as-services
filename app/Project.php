<?php
namespace App;

use App\Rules\CheckApproveProjectStatusPermissionRule;
use App\Rules\CheckSubmitProjectStatusPermissionRule;
use App\Traits\FilterByAgent;
use App\Traits\FilterByTeam;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\FilterByUser;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Class Project
 *
 * @package App
 * @property string $name
 * @property string $website
 * @property string $phone
 * @property string $invesloper
 * @property string $project_area
 * @property integer $block
 * @property integer $floor
 * @property integer $unit
 * @property decimal $from_price
 * @property decimal $to_price
 * @property string $day_of_sale
 * @property text $description
 * @property string $address
 * @property double $latitude
 * @property double $longitude
 * @property string $created_by
 * @property string $created_by_team
 * @property decimal $price
 */
class Project extends Model implements HasMedia
{
    use SoftDeletes, FilterByAgent, FilterByTeam, HasMediaTrait;


    protected $fillable = ['statistic','phrase', 'name', 'website', 'phone', 'project_area', 'block', 'floor', 'unit', 'from_price', 'to_price', 'day_of_sale', 'description', 'address', 'latitude', 'longitude', 'price', 'invesloper_id', 'created_by_id', 'created_by_team_id','project_status_id'];
    protected $appends = ['statistic_data','url','photos', 'photos_link', 'photo_urls','uploaded_photos','to_price_text','from_price_text','phrase_text'];
    protected $with = ['media'];

    public static $phraseMapping =[
        -3 =>[
            'text' => 'Dừng giữ chỗ',
            'value' => '-3',
            'color' =>''
        ],
        -2 =>[
            'text' => 'Sold out',
            'value' => '-2',
            'color' =>''
        ],
        -1 =>[
            'text' => 'Chưa mở bán',
            'value' => '-1',
            'color' =>''
        ],
            0 =>[
            'text' => 'Đang mở bán',
            'value' => '0',
            'color' =>''
        ]


    ];


    public function getStatisticDataAttribute()
    {

       if(!empty($this->statistic)){
           return json_decode($this->statistic);
       }
       return [];
    }
    public function getPhraseTextAttribute()
    {

        if(!empty(static::$phraseMapping[$this->phrase])){
            return static::$phraseMapping[$this->phrase];
        }
        if($this->phrase > 0){
            return [
                'text' => 'Đang mở bán đợt #'.$this->phrase,
                'value' => $this->phrase,
                'color' =>''
            ];

        }
        return null;
    }


    public function registerAllMediaConversions()
    {


        $this->addMediaConversion('watermark')
            ->watermark(storage_path().'/watermark/logo.png')
            ->watermarkPosition(Manipulations::POSITION_BOTTOM_RIGHT)
            ->watermarkPadding(10, 10, Manipulations::UNIT_PERCENT)
        ->setManipulations(['w' => 1024, 'h' => 1024])
            ->performOnCollections('photos')
            ->watermarkOpacity(0)
        ->nonQueued();
        $this->addMediaConversion('thumb')
            ->watermark(storage_path().'/watermark/logo.png')
            ->setManipulations(['w' => 512, 'h' => 512])
            ->performOnCollections('photos')
            ->watermarkPosition(Manipulations::POSITION_BOTTOM_RIGHT)
            ->watermarkPadding(10, 10, Manipulations::UNIT_PERCENT)
            ->watermarkOpacity(0)
            ->nonQueued();

    }

    public static function boot()
    {
        parent::boot();

        Project::observe(new \App\Observers\UserActionsObserver);
    }

    public static function storeValidation($request)
    {
        return [
            'name' => 'max:191|required',
            'website' => 'max:120|nullable',
            'phone' => 'max:191|nullable',
            'invesloper_id' => 'integer|exists:inveslopers,id|max:4294967295|required',
            'project_area' => 'max:191|nullable',
            'block' => 'integer|max:2147483647|nullable',
            'floor' => 'integer|max:2147483647|nullable',
            'unit' => 'integer|max:2147483647|nullable',
            'from_price' => 'numeric|nullable',
            'to_price' => 'numeric|nullable',
            'day_of_sale' => 'date_format:' . config('app.date_format') . '|max:191|nullable',
            'description' => 'max:65535|nullable',
            'address' => 'max:191|nullable',
            'latitude' => 'numeric|nullable',
            'longitude' => 'numeric|nullable',
            'photos' => 'nullable',
            'photos.*' => 'file|image|nullable',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable',
            'created_by_team_id' => 'integer|exists:teams,id|max:4294967295|nullable',
            'project_status_id' => 'integer|exists:project_statuses,id|max:4294967295|nullable'

        ];
    }

    public static function updateValidation($request)
    {
        return [
            'name' => 'max:191|required',
            'website' => 'max:120|nullable',
            'phone' => 'max:191|nullable',
            'invesloper_id' => 'integer|exists:inveslopers,id|max:4294967295|required',
            'project_area' => 'max:191|nullable',
            'block' => 'integer|max:2147483647|nullable',
            'floor' => 'integer|max:2147483647|nullable',
            'unit' => 'integer|max:2147483647|nullable',
            'from_price' => 'numeric|nullable',
            'to_price' => 'numeric|nullable',
            'day_of_sale' => 'date_format:' . config('app.date_format') . '|max:191|nullable',
            'description' => 'max:65535|nullable',
            'address' => 'max:191|nullable',
            'latitude' => 'numeric|nullable',
            'longitude' => 'numeric|nullable',
            'photos' => 'sometimes',
            'photos.*' => 'file|image|nullable',
            'created_by_id' => 'integer|exists:users,id|max:4294967295|nullable',
            'created_by_team_id' => 'integer|exists:teams,id|max:4294967295|nullable',
            'project_status_id' => 'integer|exists:project_statuses,id|max:4294967295|nullable'

        ];
    }

    public static function submitValidation($request)
    {

        return [
            'project_status_id' => ['required',new CheckSubmitProjectStatusPermissionRule($request->id)]
        ];
    }

    public static function approveValidation($request)
    {
        return [
            'project_status_id' => ['required',new CheckApproveProjectStatusPermissionRule($request->id)]
        ];
    }

    /**
     * Set attribute to date format
     * @param $input
     */
    public function setDayOfSaleAttribute($input)
    {
        if ($input) {
            $this->attributes['day_of_sale'] = Carbon::createFromFormat(config('app.date_format'), $input)->format('Y-m-d');
        }
    }

    /**
     * Get attribute from date format
     * @param $output
     *
     * @return string
     */
    public function getDayOfSaleAttribute($output)
    {
        if ($output) {
            return Carbon::createFromFormat('Y-m-d', $output)->format(config('app.date_format'));
        }
    }

    public function getPhotosAttribute()
    {
        return [];
    }

    public function getUploadedPhotosAttribute()
    {
        return $this->getMedia('photos')->keyBy('id');
    }


    public function getPhotoUrlsAttribute()
    {
        $photos = $this->getMedia('photos');
        if (! count($photos)) {
            return null;
        }
        $files = [];
        foreach ($photos as $file) {
            $file->url =  ($file->getUrl('watermark'));
            $files[] =$file;
        }
        return $files;

    }



    public function getToPriceTextAttribute()
    {

        return convert_number_to_words($this->to_price);

    }

    public function getFromPriceTextAttribute()
    {

        return convert_number_to_words($this->from_price);

    }



    /**
     * @return string
     */
    public function getPhotosLinkAttribute()
    {
        $photos = $this->getMedia('photos');
        if (! count($photos)) {
            return null;
        }
        $html = [];
        foreach ($photos as $file) {
            $html[] = '<a href="' . ($file->getUrl('watermark')) . '" target="_blank">' . $file->file_name . '</a>';
        }

        return implode('<br/>', $html);
    }

    public function getUrlAttribute()
    {
        return url("/admin/projects/".$this->id);

    }

    public function invesloper()
    {
        return $this->belongsTo(Invesloper::class, 'invesloper_id')->withTrashed();
    }

    public function created_by()
    {
        return $this->belongsTo(User::class, 'created_by_id');
    }

    public function created_by_team()
    {
        return $this->belongsTo(Team::class, 'created_by_team_id');
    }

    public function project_status()
    {
        return $this->belongsTo(ProjectStatus::class, 'project_status_id')->withTrashed();
    }

    public function deals()
    {
        return $this->hasMany(Deal::class, 'project_id');
    }

    public function unit_types()
    {
        return $this->hasMany(ProjectUnitType::class, 'project_id');
    }

    public function home_forms()
    {
        return $this->hasMany(HomeForm::class, 'project_id');
    }



}
