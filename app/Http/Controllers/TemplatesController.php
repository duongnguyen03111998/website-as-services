<?php

namespace App\Http\Controllers;

use App\ContentOfAgent;
use App\FavoriteProperty;
use App\Project;
use App\Property;
use App\Template;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class TemplatesController extends Controller
{
    public function index() {

        $subDomain = 'duong1';
        $contentOfAgent = ContentOfAgent::where('sub_domain', '=', $subDomain)->first();

        $template_id = $contentOfAgent->template_id;
        $template = Template::where('id', '=', $template_id)->first();

        $content = $template->template_content;

        $user_id = $contentOfAgent->user_id;
        $user = User::where('id', '=', $user_id)->first();

        $property_id = 5342;
        $property = Property::where('id', '=', $property_id)->first();

        $project_id = 11;
        $project = Project::where('id', '=', $project_id)->first();

        return view($content, ['contentOfAgent'=>$contentOfAgent,'user'=>$user,'property'=>$property,'project'=>$project]);
    }
}
