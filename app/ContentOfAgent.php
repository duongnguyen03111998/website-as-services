<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContentOfAgent extends Model
{
    protected $fillable = ['slogan', 'introduce', 'user_id', 'template_id', 'sub_domain'];
}
