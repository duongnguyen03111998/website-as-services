<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FavoriteProperty extends Model
{
    protected $fillable = ['user_id', 'property_id'];
}
