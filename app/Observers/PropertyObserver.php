<?php

namespace App\Observers;

use App\Mail\PropertySubmitEmail;
use App\Property;
use App\PropertyOperation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class PropertyObserver
{
    /**
     * Handle the property "created" event.
     *
     * @param  \App\Property  $property
     * @return void
     */
    public function created(Property $property)
    {
        //
    }

    public function updating(Property $property) {


    }


    /**
     * Handle the property "updated" event.
     *
     * @param  \App\Property  $property
     * @return void
     */
    public function updated(Property $property)
    {
        // check status to send email to duyet bds group

      //  Log::info($property->property_status);
    }

    /**
     * Handle the property "deleted" event.
     *
     * @param  \App\Property  $property
     * @return void
     */
    public function deleted(Property $property)
    {
        //
    }

    /**
     * Handle the property "restored" event.
     *
     * @param  \App\Property  $property
     * @return void
     */
    public function restored(Property $property)
    {
        //
    }

    /**
     * Handle the property "force deleted" event.
     *
     * @param  \App\Property  $property
     * @return void
     */
    public function forceDeleted(Property $property)
    {
        //
    }
}
